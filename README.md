# README #

The purpose of the project is to provide a Haskell **SOAP server** based on Yesod Framework. 

The project is in its **early phase**. Nothing special has been implemented but I still hope that at some point I'll make a soap server compatible with C# and Java clients.

Right now it can serve only predefined WSDL.

Tasks to complete in future are:

+ Add support of XSD.
+ Add support of MEX.
+ Add an embedded language for SOAP action descriptions which should be used for generation of WSDL, XSD and MEX.
+ Extend dispatching function for real dispatch and generate it by means of Template Haskell.

**Issues, ideas, pull requests are more than welcome.**